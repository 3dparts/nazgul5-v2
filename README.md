# Nazgul5 v2

A generic 3D modell for some mounts for the Nazgul5 v2 Frame.

These include the mounting for the Firefly Finder2, the DJI O3 Antenna and an ELRS Receiver platine.

The different parts can be selected at FreeCAD and exported individually or as a combined model.
